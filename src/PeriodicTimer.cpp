/*
 * PeriodicTimer.cpp
 *
 *  Created on: Jan 4, 2021
 *      Author: mehmet
 */

#include "PeriodicTimer.h"

#include <event2/event.h>
#include <event2/thread.h>

PeriodicTimer::PeriodicTimer(const boost::posix_time::time_duration& td,
								unsigned long count) :
								m_dur(td),
								m_user_count(count),
								m_cb(nullptr),
								m_is_running(false),
								_exec_thr(nullptr),
								m_base_ev(nullptr),
								m_ev(nullptr),
								m_curr_count(0),
								m_is_inf(false)
{
	//Use pthreads.
	evthread_use_pthreads();

	//Create new base event.
	m_base_ev = event_base_new();
	//Create event.
	m_ev = event_new(m_base_ev, -1, 0, &PeriodicTimer::_event_cb, this);

	//Other threads can start or stop timer.
	event_active(m_ev, -1, 0);
}

PeriodicTimer::PeriodicTimer(const boost::posix_time::time_duration& td) :
								m_dur(td),
								m_user_count(-1),
								m_cb(nullptr),
								m_is_running(false),
								_exec_thr(nullptr),
								m_base_ev(nullptr),
								m_ev(nullptr),
								m_curr_count(0),
								m_is_inf(true)
{
	//Use pthreads.
	evthread_use_pthreads();

	//Create new base event.
	m_base_ev = event_base_new();
	//Create event.
	m_ev = event_new(m_base_ev, -1, 0, &PeriodicTimer::_event_cb, this);

	//Other threads can start or stop timer.
	event_active(m_ev, -1, 0);
}

PeriodicTimer::~PeriodicTimer()
{
	//Stop before destruction.
	stop();

	//Release used memory.
	event_base_free(m_base_ev);
	event_free(m_ev);
}

bool PeriodicTimer::start_async()
{
	//Guard the execute operation.
	std::lock_guard<std::recursive_mutex> lockGuard(m_mtx);

	bool retval = false;

	//Check whether timer is already running or not.
	if(m_is_running)
	{
		//Do not start timer again. It is already ticking.
	}
	else
	{
		//Initialize event.
		struct timeval tv;
		conv_dt_timeval(m_dur, tv);

		//Add timeout to event.
		event_add(m_ev, &tv);

		//Set return value.
		retval = true;

		//Set running flag.
		m_is_running = true;

		//Start non-blocking.
		_exec_thr = new std::thread([&]()
				{
					event_base_dispatch(m_base_ev);
				});
	}

	return retval;
}

bool PeriodicTimer::start()
{
	//Guard the execute operation.
	std::lock_guard<std::recursive_mutex> lockGuard(m_mtx);

	bool retval = false;

	//Check whether timer is already running or not.
	if(m_is_running)
	{
		//Do not start timer again. It is already ticking.
	}
	else
	{
		//Set running flag.
		m_is_running = true;

		//Start blocking.
		retval = ((event_base_dispatch(m_base_ev) == 0) ? true : false);
	}

	return retval;
}

bool PeriodicTimer::stop()
{
	//Guard the execute operation.
	std::lock_guard<std::recursive_mutex> lockGuard(m_mtx);

	bool retval = false;

	if(m_is_running)
	{
		//Execute user callback function.
		m_cb(END_REASON_CANCELED);

		//Break loop.
		retval = (event_base_loopbreak(m_base_ev) == 0) ? true : false;

		//Set running flag & current tick count.
		m_is_running = false;
		m_curr_count = 0;

		term_internal();
	}
	else
	{
		//Timer is not running.
	}

	return retval;
}

bool PeriodicTimer::is_running()
{
	//Guard the execute operation.
	std::lock_guard<std::recursive_mutex> lockGuard(m_mtx);

	return m_is_running;
}

void PeriodicTimer::set_interval(boost::posix_time::time_duration& td)
{
	//Guard the execute operation.
	std::lock_guard<std::recursive_mutex> lockGuard(m_mtx);

	stop();
	m_dur = td;
}

void PeriodicTimer::set_cb(CbFnProt func)
{
	m_cb = func;
}

void PeriodicTimer::_event_cb(int fd, short event, void *arg)
{
	static_cast<PeriodicTimer*>(arg)->handle_timeout(fd, event, arg);
}

void PeriodicTimer::handle_timeout(int fd, short event, void *arg)
{
	//Guard the execute operation.
	std::lock_guard<std::recursive_mutex> lockGuard(m_mtx);

	//Increase current count.
	m_curr_count++;

	//Execute user callback function.
	m_cb(END_REASON_EXPIRED);

	if((m_is_inf) || (m_user_count > m_curr_count))
	{
		struct timeval tv;
		conv_dt_timeval(m_dur, tv);

		//Restart event.
		event_add(m_ev, &tv);
	}
	else
	{
		m_is_running = false;
		m_curr_count = 0;

		_exec_thr->detach();
	}
}

bool PeriodicTimer::conv_dt_timeval(const boost::posix_time::time_duration& td,
							timeval& tv)
{
	bool retval = true;
	tv.tv_sec = td.total_seconds();
	tv.tv_usec = td.total_microseconds() - ( td.total_seconds() * 1000 * 1000 );
	return retval;
}

void PeriodicTimer::term_internal()
{
	if(_exec_thr != nullptr)
	{
		if(_exec_thr->joinable())
		{
			_exec_thr->join();
		}
		delete _exec_thr;
	}
	else
	{
		//Execute thread is NULLPTR.
	}
}

