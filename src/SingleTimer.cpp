/*
 * SingleTimer.cpp
 *
 *  Created on: Jan 4, 2021
 *      Author: mehmet
 */

#include "SingleTimer.h"

SingleTimer::SingleTimer(const boost::posix_time::time_duration& td) :
	PeriodicTimer(td, 1)
{
}

SingleTimer::~SingleTimer()
{
}

