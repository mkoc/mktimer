/*
 * SingleTimer.h
 *
 *  Created on: Jan 4, 2021
 *      Author: mehmet
 */

#ifndef SINGLETIMER_H_
#define SINGLETIMER_H_

#include "PeriodicTimer.h"

class SingleTimer : public PeriodicTimer
{
public:

	/**
	 * Constructor.
	 * @param td refers to time duration.
	 */
	SingleTimer(const boost::posix_time::time_duration& td);

	/**
	 * Destructor.
	 */
	virtual ~SingleTimer();
};

#endif /* SINGLETIMER_H_ */
