/*
 * PeriodicTimer.h
 *
 *  Created on: Jan 4, 2021
 *      Author: mehmet
 */

#ifndef PERIODICTIMER_H_
#define PERIODICTIMER_H_

#include <boost/date_time.hpp>
#include <functional>
#include <thread>
#include <mutex>

/**
* End reason of timer.
*/
enum END_REASON
{
	END_REASON_ERR,     //!< END_REASON_ERR indicates error.
	END_REASON_CANCELED,//!< END_REASON_CANCELED indicates timer canceled by user.
	END_REASON_EXPIRED, //!< END_REASON_EXPIRED indicates timer expired.
};
	
class PeriodicTimer
{
public:

	/**
	 * Periodic timer constructor.
	 * @param td refers to time duration.
	 * @param count refers to execute count.
	 */
	PeriodicTimer(const boost::posix_time::time_duration& td, unsigned long count);

	/**
	 * Periodic timer constructor(infinite).
	 * @param td refers to time duration.
	 */
	PeriodicTimer(const boost::posix_time::time_duration& td);

	/**
	 * Destructor.
	 */
	virtual ~PeriodicTimer();

public:

	/**
	 * Type definition of callback function.
	 */
	typedef std::function<void(END_REASON)> CbFnProt;

public:

	/**
	 * Starts timer without blocking.
	 * @return true if starting successful, otherwise false.
	 */
	virtual bool start_async();

	/**
	 * Starts timer blocking.
	 * @return true if starting successful, otherwise false.
	 */
	virtual bool start();

	/**
	 * Stops timer.
	 * @return true if starting successful, otherwise false.
	 */
	virtual bool stop();

public:

	/**
	 * Accessor for running status.
	 * @return true if timer is ticking, otherwise false.
	 */
	virtual bool is_running();

public:

	/**
	 * Sets timer interval value.
	 * @param td refers to new interval.
	 * @note stops timer before setting interval.
	 */
	virtual void set_interval(boost::posix_time::time_duration& td);

public:

	/**
	 * Sets callback function.
	 * @param func refers to callback function which will be executed.
	 */
	virtual void set_cb(CbFnProt func);

protected:

	/**
	 * Holds duration.
	 */
	boost::posix_time::time_duration m_dur;

	/**
	 * Holds user registered execute count.
	 */
	unsigned long m_user_count;

protected:

	/**
	 * Holds user registered callback function.
	 */
	CbFnProt m_cb;

protected:

	/**
	 * Holds whether timer is running or not.
	 */
	std::atomic<bool> m_is_running;

private:

	/**
	 * Holds thread for async wait.
	 */
	std::thread *_exec_thr;

	/**
	 * Holds mutex for guarding internal variables.
	 */
	std::recursive_mutex m_mtx;

	/**
	 * Holds base event.
	 */
	struct event_base* m_base_ev;

	/**
	 * Holds event.
	 */
	struct event* m_ev;

	/**
	 * Holds current count.
	 */
	unsigned long m_curr_count;

	/**
	 * Holds flag for infinite status.
	 */
	bool m_is_inf;

private:

	/**
	 * Callback function for libev timer.
	 * @param fd refers to file descriptor.
	 * @param event refers to event.
	 * @param arg refers to specific arguments.
	 */
	static void _event_cb(int fd, short event, void *arg);

	/**
	 * Handles _event_cb static timeout in member function.
	 * @param fd refers to file descriptor.
	 * @param event refers to event.
	 * @param arg refers to specific arguments.
	 */
	virtual void handle_timeout(int fd, short event, void *arg);

private:

	/**
	 * Converts boost time duration to timeval.
	 * @param td posix time which will be converted from.
	 * @param tv converted timeval.
	 * @return true if converting successful, otherwise false.
	 */
	virtual bool conv_dt_timeval(const boost::posix_time::time_duration& td,
								timeval& tv);

	/**
	 * Terminates internal variables.
	 */
	virtual void term_internal();

};

#endif /* PERIODICTIMER_H_ */
